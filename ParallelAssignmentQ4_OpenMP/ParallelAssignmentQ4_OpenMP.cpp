// ParallelAssignmentQ4_OpenMP.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <stdio.h>
#include <omp.h>
#include <iostream>

#include <fstream>
#include <vector>
#include <string>

// this header file contains boost::split function 
#include <boost/algorithm/string.hpp> 
#include <boost/foreach.hpp>

int main() {
	int numThreads;
	std::cout << "Input number of threads to run (Must prepare the respective text files)\n";
	std::cin >> numThreads;
	std::vector<std::string> input;

	typedef std::vector<std::string> Tokens;
	Tokens tokens;

	int writersCount = numThreads / 2;
	int readersCount = numThreads - writersCount;

#pragma omp parallel num_threads(writersCount) // Writers only
	{
		int id = omp_get_thread_num();
		std::ifstream inFile;
		std::string filePath = "C:\\Users\\Asus\\source\\repos\\ParallelAssignmentQ4_OpenMP\\Debug\\data";
		filePath.append(std::to_string(id));
		filePath.append(".txt");
		inFile.open(filePath);
		std::string x;

		if (!inFile) {
			std::cerr << "Unable to open file " + filePath;
			exit(1);   // call system to stop
		}
		while (inFile >> x) {
#pragma omp critical
			{
				input.push_back(x);
			}
		}
	}

#pragma omp parallel num_threads(readersCount) // Writers only
	{
		int id = omp_get_thread_num();
		#pragma omp for
		for (int i = 0; i < input.size(); i++)
		{
			std::string data;
#pragma omp critical
			{ // make sure only 1 thread exectutes the critical section at a time.
				data = input[i];
				//i++;
			}
			std::cout << "\nProcess " << id << ": Element " << i << ": " << data << std::endl;
		}
	}

	printf("parallel for ends.\n");
	return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
